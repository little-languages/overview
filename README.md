# Overview

Overview of this collection of projects including the definition of each of the participant languages.  This project is the source for the [Gitlab Pages](https://little-languages.gitlab.io/overview/).


## Languages

|Name|Description  |Implementation
|----|-------------|--------------
|`LIR0`|An intermediate language used in compiling lazy functional languages. | [LRI0 Haskell](https://gitlab.com/little-languages/lir0-haskell)
|`P0`|The base procedural language supporting a number of procedures and variables defined within a single file, static typing, basic types and state mutation. | [P0 Haskell](https://gitlab.com/little-languages/p0-haskell), [P0 Kotlin](https://gitlab.com/little-languages/p0-kotlin)
