#!/bin/sh

SCRIPT_HOME=$(dirname "$0")
PROJECT_HOME="$SCRIPT_HOME/../"

echo "Translate LLT to HTML"

echo "  docs/definitions.llt"
deno run --allow-read --allow-write ./scripts/lltc.ts docs/definitions.llt -o docs/definitions.html
echo "  docs/index.llt"
deno run --allow-read --allow-write ./scripts/lltc.ts docs/index.llt -o docs/index.html

echo "  docs/miscellaneous/background.llt"
deno run --allow-read --allow-write ./scripts/lltc.ts docs/miscellaneous/background.llt -o docs/miscellaneous/background.html
echo "  docs/miscellaneous/reference.llt"
deno run --allow-read --allow-write ./scripts/lltc.ts docs/miscellaneous/reference.llt -o docs/miscellaneous/reference.html

echo "  docs/lir0/transformEBNFtoBNF.llt"
deno run --allow-read --allow-write ./scripts/lltc.ts docs/lir0/transformEBNFtoBNF.llt -o docs/lir0/transformEBNFtoBNF.html
echo "  docs/lir0/overview.llt"
deno run --allow-read --allow-write ./scripts/lltc.ts docs/lir0/overview.llt -o docs/lir0/overview.html

echo "  docs/p0/overview.llt"
deno run --allow-read --allow-write ./scripts/lltc.ts docs/p0/overview.llt -o docs/p0/overview.html
echo "  docs/p0/transformEBNFtoBNF.llt"
deno run --allow-read --allow-write ./scripts/lltc.ts docs/p0/transformEBNFtoBNF.llt -o docs/p0/transformEBNFtoBNF.html
echo "  docs/p0/AST.llt"
deno run --allow-read --allow-write ./scripts/lltc.ts docs/p0/AST.llt -o docs/p0/AST.html
echo "  docs/p0/TST.llt"
deno run --allow-read --allow-write ./scripts/lltc.ts docs/p0/TST.llt -o docs/p0/TST.html
echo "  docs/p0/EBNF.llt"
deno run --allow-read --allow-write ./scripts/lltc.ts docs/p0/EBNF.llt -o docs/p0/EBNF.html
echo "  docs/p0/BNF.llt"
deno run --allow-read --allow-write ./scripts/lltc.ts docs/p0/BNF.llt -o docs/p0/BNF.html
