/* Little Languages Template CLI
 *
 * This script is a templating system written to build the Little Languages
 * site.  The reason for writing yet another templating system is:
 *
 * - Markdown works fine for straight text with slight markup however when
 *   including maths and wanting more control of the rendering the content ends
 *   up looking like raw HTML combined with Latex.
 * - Rimu's macro's are too idiosyncratic - turning on and turning off macros
 *   whilst trying to embed Latex.  Well that was just wild.
 * - Pandoc is vast however trying to include generated code into Pandoc proved
 *   really challenging.
 *
 * So rather than battling and spending my days hacking these systems and/or
 * mechanisms it is simpler for me to knock together a super simple, highly
 * bespoke utility.
 *
 * Features:
 *
 * - Actively supports includes of either raw content or content that is to be
 *   interpreted as a template.
 * - Lightly inspired by Markdown and contains line oriented basics of Markdown.
 * - Macros defined using Rimu's notation but more predictable.
 * - Everything, and I mean everything, is obvious and under content writer's
 *   control - nothing is wrapped up in the generator itself.
 */

import { InputString } from './InputString.ts';

interface Writer {
    write(content: string): void;
}

interface MacroHandler {
    apply(args: Array<string>): string;
}

class UserMacroHandler implements MacroHandler {
    private readonly template: string;

    constructor(template: string) {
        this.template = template;
    }

    apply(args: Array<string>): string {
        const writer = new StringWriter();

        processInput(new InputString(applyArguments(this.template, args)), writer);

        return writer.toString();
    }
}

let replacements: boolean = true;
let commands: boolean = true;

const globals: { [id: string]: string } = {};

const patterns: Array<[boolean, RegExp, string]> = [];

const macros: { [id: string]: MacroHandler } = {
    chr: new class implements MacroHandler {
        apply = (args: Array<string>): string => args.length < 1 ? "" : String.fromCharCode(parseInt(args[0]));
    },
    use: new class implements MacroHandler {
        apply = (args: Array<string>): string => {
            if (args.length === 0) {
                return "";
            } else {
                const writer = new StringWriter();
                const decoder = new TextDecoder("utf-8");
                const content = decoder.decode(Deno.readFileSync(args[0]));

                processInput(new InputString(content), writer);

                return writer.toString();
            }
        }
    },
    set: new class implements MacroHandler {
        apply = (args: Array<string>): string => {
            // console.log(`set: [${args[0] || ""}] = ${args[1] || ""}`);
            globals[args[0] || ""] = args[1] || "";
            return "";
        }
    },
    get: new class implements MacroHandler {
        apply = (args: Array<string>): string => {
            // console.log(`get: [${args[0] || ""}] -> ${globals[args[0] || ""] || ""}`);
            return globals[args[0] || ""] || "";
        }
    },
    replacements: new class implements MacroHandler {
        apply = (args: Array<string>): string => {
            if (args[0] === "+") {
                replacements = true;
                return "";
            } else if (args[0] === "-") {
                replacements = false;
                return "";
            } else {
                return (replacements ? "+" : '-');
            }
        }
    },
    commands: new class implements MacroHandler {
        apply = (args: Array<string>): string => {
            if (args[0] === "+") {
                commands = true;
                return "";
            } else if (args[0] === "-") {
                commands = false;
                return "";
            } else {
                return (commands ? "+" : '-');
            }
        }
    },
    "if-then": new class implements MacroHandler {
        apply = (args: Array<string>): string => {
            if (args[0] !== "") {
                return args[1] || "";
            } else {
                return args[2] || "";
            }
        }
    },
    "if-null-else": new class implements MacroHandler {
        apply = (args: Array<string>): string => {
            if (args[0] === "") {
                return args[1] || "";
            } else {
                return args[2] || "";
            }
        }
    },
    "starts-with": new class implements MacroHandler {
        apply = (args: Array<string>): string => {
            if (String((args[1] || "")).startsWith((args[0] || ""))) {
                return "yes";
            } else {
                return "";
            }
        }
    },
    "drop-left": new class implements MacroHandler {
        apply = (args: Array<string>): string => {
            const n = parseInt("0" + (args[0] || ""));
            const input: String = args[1] || "";

            return input.slice(n);
        }
    },
    "drop-right": new class implements MacroHandler {
        apply = (args: Array<string>): string => {
            const n = parseInt("0" + (args[0] || ""));
            const input: String = args[1] || "";

            return input.slice(0, input.length - n);
        }
    },
};

function matchPattern(input: InputString): [RegExpMatchArray, string] | null {
    for (let lp = 0; lp < patterns.length; lp += 1) {
        let m = input.match(patterns[lp][0], patterns[lp][1]);

        if (m !== null && m.index === 0)
            return [m, patterns[lp][2]];
    }

    return null;
}


function applyArguments(input: string, args: Array<string>): string {
    const writer = new StringWriter();

    let lp = 0;
    const inputLength = input.length;

    while (lp < inputLength) {
        if (input[lp] === "\\" && lp < inputLength - 1) {
            writer.write(input[lp]);
            writer.write(input[lp + 1]);
            lp += 2;
        } else if (input[lp] === "$") {
            lp += 1;

            const start = lp;

            while (input[lp] >= "0" && input[lp] <= "9") {
                lp += 1;
            }

            const argIndex = parseInt("0" + input.slice(start, lp));

            if (argIndex < args.length) {
                writer.write(args[argIndex]);
            }
        } else {
            writer.write(input[lp]);
            lp += 1;
        }
    }

    return writer.toString();
}


function processMacro(input: InputString, writer: Writer): InputString {
    let lp = 1;
    while (input.at(lp) !== " " && input.at(lp) !== "\n" && input.at(lp) !== '|' && input.at(lp) !== '}' && input.at(lp) !== "") {
        lp += 1;
    }
    const name: string = input.slice(1, lp).trim();

    const args: Array<string> = [];
    while (input.at(lp) === " " || input.at(lp) === "\n") {
        lp += 1;
    }
    if (input.at(lp) === "|") {
        lp += 1;

        const pushArgument = (arg: string): void => {
            args.push(processString(arg));
        }

        let startArg = lp;
        let nesting = 0;
        while (true) {
            if (input.at(lp) === "|" && nesting === 0) {
                pushArgument(input.slice(startArg, lp));
                lp += 1;
                startArg = lp;
            } else if (input.at(lp) === "}" && nesting === 0) {
                pushArgument(input.slice(startArg, lp));
                lp += 1;
                break;
            } else if (input.at(lp) === "}" && nesting > 0) {
                lp += 1;
                nesting -= 1;
            } else if (input.at(lp) === "{") {
                lp += 1;
                nesting += 1;
            } else if (input.at(lp) === "\\") {
                lp += 2;
            } else if (input.at(lp) === "") {
                pushArgument(input.slice(startArg, lp));
                break;
            } else {
                lp += 1;
            }
        }
    } else if (input.at(lp) === "}") {
        lp += 1;
    }

    const macro: MacroHandler = macros[name];

    if (macro === undefined) {
        writer.write(input.take(1));
        return input.advance(1);
    } else {
        writer.write(macro.apply(args));
        return input.advance(lp);
    }
}


function processCommand(input: InputString, writer: Writer): InputString {
    let lp = 1;

    if (input.slice(lp, lp + 2) === "^/") {
        lp += 2;
        while (true) {
            if (input.at(lp) === "") {
                break;
            } else if (input.at(lp) === "\\") {
                lp += 2;
            } else if (input.at(lp) === "/") {
                lp += 1;
                break;
            } else {
                lp += 1;
            }
        }
        const pattern = new RegExp(input.slice(3, lp - 1));

        const handlerStart = lp;

        while (input.at(lp) !== "" && input.at(lp) !== "\n") {
            lp += 1;
        }

        const handler = input.slice(handlerStart, lp).trim();
        patterns.push([true, pattern, handler]);
    } else if (input.at(lp) === "/") {
        lp += 1;
        while (true) {
            if (input.at(lp) === "") {
                break;
            } else if (input.at(lp) === "\\") {
                lp += 2;
            } else if (input.at(lp) === "/") {
                lp += 1;
                break;
            } else {
                lp += 1;
            }
        }
        const pattern = new RegExp(input.slice(2, lp - 1));

        const handlerStart = lp;

        while (input.at(lp) !== "" && input.at(lp) !== "\n") {
            lp += 1;
        }

        const handler = input.slice(handlerStart, lp).trim();

        patterns.push([false, pattern, handler]);
    } else if (input.slice(lp, lp + 4) === "def ") {
        lp += 4;
        while (input.at(lp) !== " ") {
            lp += 1;
        }
        const pattern = input.slice(5, lp);
        const handlerStart = lp;

        while (input.at(lp) !== "" && input.at(lp) !== "\n") {
            lp += 1;
        }

        macros[pattern] = new UserMacroHandler(input.slice(handlerStart, lp).trim());
    } else if (input.slice(lp, lp + 4) === "use ") {
        lp += 4;
        while (input.at(lp) !== "" && input.at(lp) !== "\n") {
            lp += 1;
        }

        const name = input.slice(4, lp).trim();

        const decoder = new TextDecoder("utf-8");
        const content = decoder.decode(Deno.readFileSync(name));

        processInput(new InputString(content), writer);
    } else {
        while (input.at(lp) !== "" && input.at(lp) !== "\n") {
            lp += 1;
        }
    }

    if (input.at(lp) === "\n") {
        return input.advance(lp + 1);
    } else {
        return input.advance(lp);
    }
}

class StringWriter implements Writer {
    private result: string;

    constructor() {
        this.result = "";
    }

    write(content: string) {
        this.result = this.result + content;
    }

    toString(): string {
        return this.result;
    }
}

function processInput(input: InputString, writer: Writer, processCommands: boolean = true): void {
    while (!input.endOfInput()) {
        const first = input.take(1);

        if (first === "{")
            input = processMacro(input, writer);
        else if (commands && input.position()[1] === 1 && first === "." && processCommands)
            input = processCommand(input, writer);
        else if (replacements) {
            const x = matchPattern(input)
            if (x == null) {
                writer.write(first);
                input = input.advance(1);
            } else {
                const [args, handler] = x;

                processInput(new InputString(applyArguments(handler, args)), writer);

                input = input.advance(args[0].length);
            }
        } else {
            writer.write(first);
            input = input.advance(1);
        }
    }
}

function processString(input: string): string {
    const writer = new StringWriter();

    processInput(new InputString(input), writer, false);

    return writer.toString();
}


function readTextFile(name: string): Promise<String> {
    const decoder = new TextDecoder("utf-8");
    return Deno.readFile(name).then(t => decoder.decode(t));
}

function writeTextFile(name: string, content: string): Promise<void> {
    const encoder = new TextEncoder();

    return Deno.writeFile(name, encoder.encode(content));
}

function reportError(msg: String) {
    console.error(`Error: ${msg}`);
    Deno.exit(-1);
}

interface Options {
    inputFile: String | null,
    outputFile: String | null
}

let options: Options = {
    inputFile: null,
    outputFile: null
};


function main(args: Array<String>) {
    let lp = 0;
    while (lp < args.length) {
        if (args[lp] == '-o') {
            lp += 1;
            if (lp < args.length) {
                options.outputFile = args[lp];
                lp += 1;
            } else
                reportError("-o requires an argument for file name");
        } else if (options.inputFile !== null) {
            reportError("Only a single input file can be supplied");
        } else {
            options.inputFile = args[lp];
            lp += 1;
        }
    }

    if (options.inputFile === null)
        reportError("An input file is required");
    else if (options.outputFile === null)
        reportError("An output file is required");
    else
        readTextFile(options.inputFile as string).then(input => {
            let result = new StringWriter();
            processInput(new InputString(input), result);
            return result.toString();
        }).then(output => writeTextFile(options.outputFile as string, output));
}

main(Deno.args);