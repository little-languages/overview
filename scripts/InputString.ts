export class InputString {
    private readonly input: String;
    private readonly row: number;
    private readonly column: number;

    constructor(input: String, row: number = 1, column: number = 1) {
        this.input = input;
        this.row = row;
        this.column = column;
    }

    advance(n: number): InputString {
        if (n === 0)
            return this;
        else {
            let lp = 0;
            let row = this.row;
            let column = this.column;

            while (lp < n) {
                if (lp < this.input.length) {
                    if (this.input[lp] === '\n') {
                        row += 1;
                        column = 1;
                    } else {
                        column += 1;
                    }
                }
                lp += 1;
            }

            return new InputString(this.input.slice(n), row, column);
        }
    }

    take(n: number): string {
        return this.input.slice(0, n);
    }

    endOfInput(): boolean {
        return this.input === "";
    }

    match(mustBeStartOfLine: boolean, regexp: RegExp): RegExpMatchArray | null {
        if (mustBeStartOfLine && this.column !== 1)
            return null;
        else
            return this.input.match(regexp);
    }

    toString(): String {
        return `[${this.input}, ${this.input.length}, ${this.row}, ${this.column}]`;
    }

    at(offset: number): string {
        return offset < this.input.length ? this.input[offset] : "";
    }

    slice(start: number, end: number | undefined): string {
        return this.input.slice(start, end);
    }

    position(): [number, number] {
        return [this.row, this.column]
    }
}
